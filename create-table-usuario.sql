
// Crear Tabla
CREATE TABLE usuario (
	username VARCHAR(16) NOT NULL PRIMARY KEY,
	nombre VARCHAR(32) NOT NULL,
	apellido VARCHAR(32) NOT NULL,
	fecha_nacimiento DATE NOT NULL,
	password VARCHAR(128) NOT NULL
);

// Insertar datos en  Tabla
INSERT INTO `dai5501`.`usuario` (
`username` ,
`nombre` ,
`apellido` ,
`fecha_nacimiento` ,
`password` 
)
VALUES (
'admin', 'admin', 'admin', '2019-10-10', 'admin'
);