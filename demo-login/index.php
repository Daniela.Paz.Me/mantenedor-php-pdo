<?php 
require_once "conf/Config.php";
require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/model/Usuario.php";
require_once ROOT_PATH ."/controller/SessionController.php";


$controller = new SessionController();

if(!$controller->estaAutenticado()) {
    header("Location: login.php");
    die();
}

// Cereación de usuario
// Valida si se encontra al formulario por POS (se enviaron datos)
if($_SERVER['REQUEST_METHOD'] == 'POST'){

// Valida si se presiono el boton "Guardar" que esta en la pagina "Insertar".
    if($_POST["btnGuardar"]=!NULL){

        // Rescata los datos del formulario
        $usuario = new Usuario();

        $usuario-> setUsername($_POST["txtUsuario"]);
        $usuario->setNombre($_POST["txtNombre"]);
        $usuario->setApellido($_POST["txtApellido"]);
        $usuario->setFechaNacimiento($_POST["txtFecha"]);
        $usuario->setPassword($_POST["txtContrasena"]);

        // Imprime el usuario obtenido
        /* echo "Usuario obtenido: " . $usuario->getUsername() . " - " . $usuario->getNombre() . " - "
        . $usuario->getApellido() . " - " . $usuario->getFechaNacimiento() . " - " . $usuario->getPassword();
        */

        // Ejecuta consulta del controlador
        $controller->insertarUsuario($usuario);

        // Muestra mensaje segun sea la ejecución
        if($controller){
        echo '</br>';
        echo '<script language="javascript">alert("Usuario Insertado");</script>';

        }else{
        echo '</br>';
        echo '<script language="javascript">alert("Usuario NO Insertado");</script>';
        }
    }
  }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro de Usuarios</title>
        
        <link rel="stylesheet" type="text/css" href="librerias/bootstrap4/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php require_once "Insertar.php" ?>

        <script src="librerias/bootstrap4/jquery-3.4.1.min.js"></script>
        <script src="librerias/bootstrap4/popper.min.js"></script>
        <script src="librerias/bootstrap4/bootstrap.min.js"></script>
        <script src="librerias/sweetalert.min.js"></script>
        <script src="js/crud.js"></script>


        <script type="text/javascript">
           // mostrar();
        </script>
        
        <script>
            mostrar();
            function modificar(usuario) {
                location.href='editar.php?username='+usuario;
            }
        
            function eliminar(usuario) {
                document.formularioEliminar.username.value=usuario;
                formularioEliminar.submit();
            }

        </script>
        
    </head>
    <body>

    <h1>Demo Admin Login</h1>

    [<a href="logout.php" >Logout <?= $controller->getNombreUsuarioAutenticado() ?></a>]


       <!-- Carta que muestra lista de usuarios -->
	<div class="container">
		<div class="row">
			<h2>Mantenedor usuario</h2>
			<div class="col-sm-12">
				<div class="card text-left">
					<div class="card-header">
						<ul class="nav nav-tabs card-header-tabs">
							<li class="nav-item">
								<a class="nav-link active" href="#">Usuarios</a>
							</li>
						</ul>
					</div>

                    <!-- Ejecuta Modal que levanta pagina "Insertar" -->
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<span class="btn btn-primary" data-toggle="modal" data-target="#insertarModal">
									<i class="fas fa-plus-circle"></i> Nuevo Usuario
								</span>
							</div>
						</div>
						<hr>

                        <!-- Llama a la lista y la muestra -->
                        <?php 
                            $usuarios = $controller->getUsuarios();
                        ?>

						<div class="row col-sm-12">
							<div class="col-sm-12">
								<table class="table table-dark col-sm-12">
									<thead>
										<tr class="font-weight-bold">
											<td>USERNAME</td>
                                            <td>NOMBRE</td>
											<td>APELLIDO</td>
											<td>FECHA_NACIMIENTO</td>
											<td>MODIFICAR</td>
											<td>ELIMINAR</td>
										</tr>
									</thead>
									<tbody>
                                    <?php
                                        foreach($usuarios as $user) {
                                            /* @var $user Usuario */
                                        ?>

										<tr>
											<td><?= $user->getUsername() ?></td>
											<td><?= $user->getNombre() ?></td>
											<td><?= $user->getApellido() ?></td>
											<td><?= $user->getFechaNacimiento() ?></td>
											
											<td>
												<span class="btn btn-warning btn-sm center" onclick="obtenerDatos()" data-toggle="modal" data-target="#actualizarModal">
													<i class="fas fa-edit"></i>
												</span>
												
											</td>
											<td>
												<span class="btn btn-danger center" onclick="eliminarDatos()">
													<li class="fas fa-trash-alt"></li>
												</span>
											</td>
										</tr>
                                        <?php
                                            }
                                        ?> 
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    </body>
</html>


