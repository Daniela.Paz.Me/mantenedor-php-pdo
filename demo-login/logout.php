<?php

require_once "conf/Config.php";
require_once ROOT_PATH ."/controller/SessionController.php";

$controller = new SessionController();
$controller->logout();

header("Location: login.php");
die();
