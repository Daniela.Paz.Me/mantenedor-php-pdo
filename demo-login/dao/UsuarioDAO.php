<?php

require_once ROOT_PATH ."/dao/GenericDAO.php";

interface UsuarioDAO extends GenericDAO {
   
    function getByUsername($username);
       
}
