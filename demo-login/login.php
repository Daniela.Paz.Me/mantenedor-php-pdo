<?php 
require_once "conf/Config.php";
require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/model/Usuario.php";
require_once ROOT_PATH ."/controller/SessionController.php";

$controller = new SessionController();
$error = false;

if($controller->estaAutenticado()) {
    header("Location: index.php");
    die();
}

if($controller->estaEnviandoCredenciales()) {
    
    if($controller->autenticarUsuario($_POST["username"], $_POST["clave"])) {
        header("Location: index.php");
        die();
    }
    
    $error = true;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro de Usuarios</title>
        
        <script>
                 

        </script>
        
    </head>
    <body>
        <h1>Demo Login</h1>

        <?php 
            if($error) {
                echo '<div class="error">Credenciales inválidas</div>';
            }
        ?>
        
        <form action="login.php" method="POST" >
            <table>           
                    <tr>
                        <td>Usuario:</td>                    
                        <td>
                            <input type="text" 
                                   name="username" 
                                   value="" />
                        </td>
                    </tr> 
                    <tr> 
                        <td>Clave:</td> 
                        <td>
                            <input type="password" 
                                   name="clave" 
                                   value=""  />
                        </td>
                    </tr>  

                    <tr> 
                        <td colspan="2">
                            <input type="submit" 
                                   name="submit" 
                                   value="Acceder"  />
                        </td>
                    </tr>
            </table>        
        </form>
    </body>
</html>
